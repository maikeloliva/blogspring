# Leeme #

Este archivo documenta los pasos hacer para arrancar una aplicación de blog.

### ¿Para qué es este repositorio? ###

* Este repositorio, se utilizará para realizar un blog........
* Se estará realizando tareas durante toda la semana.
* Las tareas indicadas anteriormente se subiran al repositorio diariamente.

### ¿Cómo me preparo? ###

* Se ha estado aprendiendo Java desde la base con fundamentals y Programming.

### Contribution guidelines ###

* Se acepta critica constructiva sobre el diseño de la aplicación.
* Se podrá contribuir a la correción de escritura de código.

### ¿Con quién hablo? ###

* Compañeros de clase, profesor y compañeros con conocimientos en la creación de aplicaciones 
* Foros, comunidades...

--------------------------------------(English)-----------------------------------------------------

# Readme #

This file documents the steps to take to start a blog application.

### What is this repository for? ###

* This repository will be used to make a blog.
* Work will be done throughout the week.
* The tasks indicated above will be uploaded to the repository daily.

### How do I prepare? ###

* It has been learning Java from the base with Fundamentals and Programming.

### Contribution guidelines ###

* Constructive criticism about the design of the application is accepted.
* You can contribute to the code writing correction.

### With whom I speak? ###

* Classmates, teacher and colleagues with knowledge in the creation of applications
* Forums, communities ...