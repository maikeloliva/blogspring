package com.corenetworks.hibernate.spring.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.ColumnTransformer;
import org.hibernate.annotations.CreationTimestamp;

@Entity
public class User {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)//genera automatico
	private long id;
	
	@Column(name="NAME")
	private String nombre;
	
	@Column
	private String ciudad;
	
	@Column
	private String email;
	
	@Column
	@ColumnTransformer(write=" MD5(?) ")//hace que la contraseña se encripte 
	private String password;
	
	public User() {
		
	}
	
	public User(String nombre, String ciudad, String email, String password) {
		super();
		this.nombre = nombre;
		this.ciudad = ciudad;
		this.email = email;
		this.password = password;
	}
	
	@Column
	@CreationTimestamp//genera la fecha 
	private Date fechaAlta;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getFechaAlta() {
		return fechaAlta;
	}
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	@Override
	public String toString() {
		return " <tr>"+ 
			    "<td style='border: 1px solid black; width: 20px; padding: 15px;'>" + 
			    getId() + "</td>" + 
			  "<td style='border: 1px solid black; width: 20px; padding: 15px;'>"  + getNombre() + "\t" + "</td>" + 
			
			 
		    "<td style='border: 1px solid black; width: 20px; padding: 15px;'>" + 
		    getCiudad() + "</td>" + 
		  "<td style='border: 1px solid black; width: 20px; padding: 15px;'>"  + getEmail() + "\t" + "</td>" + 
		 
		
	    "<td style='border: 1px solid black; width: 20px; padding: 15px;'>" + 
	    getPassword() + "</td>" + 
	  "<td style='border: 1px solid black; width: 50px; padding: 25px;'>"  + getFechaAlta() + "\t" + "</td>" + 
	" </tr>";
	}
	
	
	
}
