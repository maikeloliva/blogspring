package com.corenetworks.hibernate.spring.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


import com.corenetworks.hibernate.spring.dao.UserDao;
import com.corenetworks.hibernate.spring.model.User;

@Controller
public class MainController {
	@Autowired
	private UserDao usuario;
	@GetMapping(value="/")
	 public String welcome(Model modelo) {
		 
		 
		 
		 return "index";
	 }
	@RequestMapping(value="/list")
	@ResponseBody
	public String getAll(){
		List<User> lUsuario = new ArrayList<>();
		lUsuario = usuario.getAll();
		
		return  "<head>"+
		        "<title>Servlet SrvProfesor</title>"+         
		        "</head>"+
		         "<body style='background-color: #FEFFDF '>"+
		         "<h1 style='text-align:center;'>Tabla Usuarios"+"</h1>"+
		         "<table style='border: 1px solid black; border-collapse: collapse; text-align: center; margin: 0 auto; width: 700px;'>"+
		         
		         "<tr>" +"<td style='border: 1px solid black; width: 50px; padding: 15px;'>"+lUsuario.toString()+"</td>"+
		         
		 "</tr>"+ 
		        "</table>"+
		       "</body>";
	}
		
}
