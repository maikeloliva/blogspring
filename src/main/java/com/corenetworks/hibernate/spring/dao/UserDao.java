package com.corenetworks.hibernate.spring.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.corenetworks.hibernate.spring.model.User;

@Repository
@Transactional
public class UserDao {

	@PersistenceContext
	private EntityManager entityManager;
	/*
	 * almacena el usuario en la base de datos
	 */
	public void create(User usuario) {
		entityManager.persist(usuario);
		return;
	}
	@SuppressWarnings("unchecked")
	public List<User> getAll(){
		return entityManager.createQuery("select u from User u")
				.getResultList();
	}
	
}
